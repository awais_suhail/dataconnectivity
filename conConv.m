filename = 'connectors_bmw.mat';

comp_table = readtable('components_bmw.csv');

for p = 1:numel(comp_table.variable)
    path = squeeze(comp_table.path{p});

    liste = dir(path);
    files = {liste.name};
    files = files(3:end);
    abcd = [];
    for k=1:numel(files)
        abcd{k} = abcdparameters([path '/' files{k}]);
    end


    temp.name = comp_table.name{p};
    temp.type = comp_table.type{p};
    temp.system = comp_table.system{p};
    temp.parameters = regexp(comp_table.parameters{p}, '([^,]*)','tokens');
    temp.parameter_names = regexp(comp_table.parameter_names{p}, '([^,]*)','tokens');

    temp.abcd = abcd;
    temp.abcd_parameters = str2num(comp_table.parameter_values{p});
    temp.cable = {'PCB', 'RTK031'};
    temp.cable_names = {'PCB', 'Generic RTK031'};
    
    eval([comp_table.variable{p} ' = temp;']);
end

save(filename, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
