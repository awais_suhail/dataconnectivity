function LoadCableData()
%LOADCABLEDATA Summary of this function goes here
%   Detailed explanation goes here

filenames = flipud(dir('CableData/**/*.json'));

global cables;
cables = struct;

global CableTable;
CableTable = cell2table(cell(0,9),'VariableNames',{'uid','manid','man','name','type','fmax','parameters','format','impedance'});

row = {1,'ideal','', 'ideal', 'coax', 1e12, {struct('c1', 0, 'c2', 0)}, 'cfinal', {50}};
CableTable = [CableTable;row];

uid=2;

for filecnt = 1:numel(filenames)
    fname = [filenames(filecnt).folder '/' filenames(filecnt).name];

    fid = fopen(fname);
    raw = fread(fid, Inf, "*char")';
    fclose(fid);
    try
        val = jsondecode(raw);
    catch exception
        disp(['WARNING: JSON parser error in file' fname ': ' exception.message]);
        continue
    end
    if val.dbversion == 1
        cablenames = fieldnames(val.cables);
        for cablecnt=1:length(cablenames)
            row = {uid,val.manid,val.manufacturer, ...
                val.cables.(cablenames{cablecnt}).name ...
                val.cables.(cablenames{cablecnt}).type ...
                val.cables.(cablenames{cablecnt}).fmax ...
                val.cables.(cablenames{cablecnt}).data ...
                val.cables.(cablenames{cablecnt}).format ...
                val.cables.(cablenames{cablecnt}).impedance ...
                };
            CableTable = [CableTable;row];
            uid = uid + 1;
        end
    else
        disp(['Unsupported database version of file ' fname]);
    end
end

for filecnt = 1:numel(filenames)
    fname = [filenames(filecnt).folder '/' filenames(filecnt).name];

    fid = fopen(fname);
    raw = fread(fid, Inf, "*char")';
    fclose(fid);

    try
        val = jsondecode(raw);
    catch exception
        disp(['WARNING: JSON parser error in file' fname ': ' exception.message]);
        continue
    end

    if val.dbversion == 1
        namesuggestion = val.manid;

        k = 0;
        while isfield(cables, namesuggestion)
            namesuggestion = [val.manid '_' num2str(k)];
            k = k + 1;
        end

        eval(['cables.' namesuggestion ' = val;']);
    else
        disp(['Unknown database version of file ' fname]);
    end

end

global CableTableBase;
CableTableBase = CableTable;
CableTable = CableTable(strcmp(CableTable.type,'coax'),:);
%end

