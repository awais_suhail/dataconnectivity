% Test file for cable parameter determination with the cableparams function.

clear all

% f = [.1 1.5 2.25]*1e9;
% a = [4 18.4 25.6];

cables = struct;

filenames = dir('CableData/**/*.json');

for filecnt = 1:numel(filenames)
    fname = [filenames(filecnt).folder '/' filenames(filecnt).name];

    fid = fopen(fname);
    raw = fread(fid, Inf, "*char")';
    fclose(fid);

    try
        val = jsondecode(raw);
    catch exception
        disp(['JSON parser error in file ' fname]);
        continue
    end

    if val.dbversion == 1
        namesuggestion = val.manid;

        k = 0;
        while isfield(cables, namesuggestion)
            namesuggestion = [val.manid '_' num2str(k)];
            k = k + 1;
        end

        eval(['cables.' namesuggestion ' = val;']);
    else
        disp(['Unknown database version of file ' fname]);
    end

end

cable_makes = fieldnames(cables);

%for k=1:numel(cable_makes)
%    eval('cables.cable_makes{k}.cables')
%end

cable_makes = fieldnames(cables)'

for cable_make = cable_makes
    for allcables = fieldnames(cables.(cable_makes{1}).cables)'
        cables.(cable_makes{1}).cables.(allcables{1});
    end
end

%cables.(cable_makes{:})
        
% c = cableparams(f, a)
% 
% f_fit = (0.01:1e-3:10)*1e9;
% a_fit = c(1).*sqrt(f_fit) + c(2).*f_fit + c(3)./sqrt(f_fit);
% 
% plot(f_fit, a_fit, f, a, '*');
