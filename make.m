addpath('sub');

a = git('describe');
fid = fopen('version.txt','wt');
if strcmp(a, 'fatal: No names found, cannot describe anything.')
    fprintf(fid, 'unknown version');
else
    fprintf(fid, a);
end
fclose(fid);

applicationCompiler -package TEApp

pause

mkdir(['temp/' a]);
movefile('TEApp/for_redistribution/MyAppInstaller_mcr.exe', ['temp/' a '/' a '-installer.exe']);

git(['archive ' a ' --format=tar.gz > temp/' a '/' a '-src.tar.gz']);
system(['c:/Programme/7-Zip/7z a temp/' a '/' a '.7z TEApp/for_redistribution_files_only']);
oldFolder = cd(['temp/' a]);
system(['C:/Programme/Git/usr/bin/md5sum *' a '* >> md5sum.txt']);
cd(oldFolder);
