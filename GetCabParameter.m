% GetCabParameter.m, version 0.1, 02-16-2018
% bbergner@te.com, eric.dibiaso@te.com
%
% calculates s-parameter matrix for cables
% uses method decribed in the TechCon2013 paper - equation (13)
%
% all dimensions given and expected in SI base units

% arguments

    %      CAB.c1      [sqrt(s)/m] attenuation coefficient c1
    %      CAB.c2      [s/m] attenuation coefficient c2
    %      CAB.vp      [m/s] phase or propagation velocity
    %      CAB.Zw      [ohm] cable impedance
    %      CAB.len     [m] cable length
    %      CAB.Z0      [ohm] port impedance for normalization
    %      CAB.THA_mul []   multiplier for Temp,Humidity, Aging
    %      CAB.cab_mul []   multiplier for different Guage wire
    %      Freq        frequency vector [Hz]

% return values

    %      abcdcab     ABCD-parameters object


function abcdcab = GetCabParameter(CAB, freq)
    % renormalize c1 and c2 to a1 and a2 according equation (13)
    % (alpha 1 and alpha 2 in the TechCon2013 paper)
    e = 2.718281828; 
    a1=-CAB.c1/20/log10(e);
    a2=-CAB.c2/20/log10(e);

    % calculate cable s-parameter
    s_params(1,1,1:length(freq)) = 0;
    s_params(2,1,:) = exp(-CAB.len*(CAB.cab_mul*sqrt(freq)*a1 +   CAB.THA_mul*freq   * a2+1i*2*pi*freq  ./CAB.vp));
    s_params(1,2,:) = s_params(2,1,:);
    s_params(1,1,:) = 0;

    % normalize to port impedances
    cable_data_portA = abcdparameters(s2abcd(GetImpStepParameter(CAB.Z0, CAB.Zw, length(freq)), 50), freq);
    cable_data_temp = abcdparameters(s2abcd(s_params, 50), freq);
    cable_data_portB = abcdparameters(s2abcd(GetImpStepParameter(CAB.Zw, CAB.Z0, length(freq)), 50), freq);
    
    abcdcab = casc_TS_single(freq, casc_TS_single(freq, cable_data_portA, cable_data_temp), cable_data_portB); 
end
