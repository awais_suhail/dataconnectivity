function connector_db = loadConnectorDB(system)

x = load('connectors_bmw.mat');

%connector_db;

con_cnt = 1; % separate from loop iteration to allow for easy extension to
             % multi-file loading

current_fields = fieldnames(x);
for k = 1:numel(current_fields)
    connector_db.(['con_' num2str(con_cnt)]) = x.(current_fields{k});
    con_cnt = con_cnt + 1;
end

% separate loop to ease multi system extension
current_fields = fieldnames(connector_db);
for k = 1:numel(current_fields)
    if ~strcmp(connector_db.(current_fields{k}).system, system)
        connector_db = rmfield(connector_db, current_fields{k});
    end
end

clear('x', 'k', 'con_cnt', 'current_fields');

end
