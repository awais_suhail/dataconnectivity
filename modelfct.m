function y = modelfct (x, f, a)
% Copyright (c) 2019 Christian Mandel
% 
% This software is property of TE connectivity. Do not share or redistribute.

% modelfct  Helper function that implements the equation system for the cableparams function. 

  y = zeros (3, 1);
  y(1) = x(1)*sqrt(f(1)) + x(2)*f(1) + x(3)/sqrt(f(1)) - a(1);
  y(2) = x(1)*sqrt(f(2)) + x(2)*f(2) + x(3)/sqrt(f(2)) - a(2);
  y(3) = x(1)*sqrt(f(3)) + x(2)*f(3) + x(3)/sqrt(f(3)) - a(3);
 
endfunction
