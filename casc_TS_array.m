function Scasc=casc_TS_array(freq, header_left, cable1, header_right, inliner1, cable2, inliner2, cable3, inliner3, cable4, inliner5, cable6)
    
    if exist('inliner4')==1
        for i = 1:length(freq)
            ABCDcasc(:,:,i) = header_left.Parameters(:,:,i)*cable1.Parameters(:,:,i)*inliner1.Parameters(:,:,i)*cable2.Parameters(:,:,i)*inliner2.Parameters(:,:,i)*cable3.Parameters(:,:,i)*inliner3.Parameters(:,:,i)*cable4.Parameters(:,:,i)*inliner4.Parameters(:,:,i)*cable5.Parameters(:,:,i)*header_right.Parameters(:,:,i);
        end
    elseif exist('inliner3')==1   
        for i = 1:length(freq)
            ABCDcasc(:,:,i) = header_left.Parameters(:,:,i)*cable1.Parameters(:,:,i)*inliner1.Parameters(:,:,i)*cable2.Parameters(:,:,i)*inliner2.Parameters(:,:,i)*cable3.Parameters(:,:,i)*inliner3.Parameters(:,:,i)*cable4.Parameters(:,:,i)*header_right.Parameters(:,:,i);
        end
    elseif exist('inliner2')==1   
        for i = 1:length(freq)
            ABCDcasc(:,:,i) = header_left.Parameters(:,:,i)*cable1.Parameters(:,:,i)*inliner1.Parameters(:,:,i)*cable2.Parameters(:,:,i)*inliner2.Parameters(:,:,i)*cable3.Parameters(:,:,i)*header_right.Parameters(:,:,i);
        end
    elseif exist('inliner1')==1   
        for i = 1:length(freq)
            ABCDcasc(:,:,i) = header_left.Parameters(:,:,i)*cable1.Parameters(:,:,i)*inliner1.Parameters(:,:,i)*cable2.Parameters(:,:,i)*header_right.Parameters(:,:,i);
        end
    else
        for i = 1:length(freq)
            ABCDcasc(:,:,i) = header_left.Parameters(:,:,i)*cable1.Parameters(:,:,i)*header_right.Parameters(:,:,i);
        end
    end
    ABCDcasc = abcdparameters(ABCDcasc,freq);
    Scasc = sparameters(ABCDcasc);
end