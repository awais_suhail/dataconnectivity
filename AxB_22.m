% AxB_22.m, version 0.1, 2-16-2018
% bbergner@te.com, eric.dibiaso@te.com
%
% cascades two t-parameter matrices, expects 2x2 matrix vector
%
% all dimensions given and expected in SI base units

% arguments

    %  A, B      two 2x2 x NF matrices

% return values

    %  C      2x2 matrix A*B for each frequency step iy:
    %             C11=C((iy*2-1),1)     C12=C((iy*2-1),2)
    %             C21=C((iy*2),1)       C22=C((iy*2),2)

function C=AxB_22(A,B)
    dim=size(A);
    for iy=1:(dim(1,1)/2)
        C((iy*2-1):(iy*2),1:2)=A((iy*2-1):(iy*2),1:2)*B((iy*2-1):(iy*2),1:2);
    end
end
