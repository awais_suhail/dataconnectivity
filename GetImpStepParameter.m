% GetImpStepParameter.m, version 0.1, 2-16-2108
% bbergner@te.com, eric.dibiaso@te.com
%
% calculates s-parameter matrix for an impedance step ZA-to-ZB
%
% all dimensions given and expected in SI base units

% arguments

    %  ZA      [ohm], left side impedance
    %  ZB      [ohm], right side impedance
    %  NF      number of frequency steps

% return values

    %      S         complex 2-port s-parameter matrix for each frequency
    %                step iy:
    %                    S11=S((iy*2-1),1)     S12=S((iy*2-1),2)
    %                    S21=S((iy*2),1)       S22=S((iy*2),2)

function s_params = GetImpStepParameter(ZA, ZB, NF)

S_ = [((ZB-ZA)/(ZB+ZA)), (2*sqrt(ZA*ZB)/(ZA+ZB)); ...
     (2*sqrt(ZB*ZA)/(ZB+ZA)), ((ZA-ZB)/(ZA+ZB))];

s_params(1,1,1:NF) = S_(1,1);
s_params(1,2,:) = S_(1,2);
s_params(2,1,:) = S_(2,1);
s_params(2,2,:) = S_(2,2);

end
