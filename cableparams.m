function coefvector = cableparams (f, a)
% Copyright (c) 2019 Christian Mandel
% 
% This software is property of TE connectivity. Do not share or redistribute.

% cableparams  Compute cable model parameters based on 3 frequency attenuation pairs.
%
%   c = cableparams(f, a)
%
%   f is a vector with 3 double numbers describing the 3 frequency points of
%   frequency attenuation pairs. The 3 element double vector a holds the
%   corresponding attenuation in dB.
%
%   The underlying cable model is based on the equation
%
%       a(f) = c1 * sqrt(f) + c2 * f + c3/sqrt(f)
%
%   with
%
%       [f] = Hz, f = [f1, f2, f3]
%       [a] = dB, a = [a1, a2, a3]
%
%   and (fn, an) being a frequency attenuation pair.
%
%   The frequency vector is preconditioned in a way that it does not exceed
%   computation accuracy limits for reasonable inputs in the MHz to several GHz
%   range. The output is scaled for frequency input in Hz. The algorithm itself
%   is frequency agnostic, but due to the preconditioning it makes sense to
%   stick to these input units.

  % check for malformed input
  if ~isvector(f) | length(f) ~= 3 | ~isa(f, 'double')
    disp('f is not a vector or has wrong dimensions or data format. Please check!')
    return
  end

  if ~isvector(a) | length(a) ~= 3 | ~isa(a, 'double')
    disp('a is not a vector or has wrong dimensions or data format. Please check!')
    return
  end

  % compute cable model parameters, preconditioned to GHz input unit
  [coefvector, fval, info] = fsolve (@(x) modelfct (x, f./1e9, a), [1; 1; 1]);
  % decondition to Hz input
  coefvector = diag([1/sqrt(1e9), 1e-9, sqrt(1e9)]) * coefvector;

  f= (0:.01:10) * 1e9;
  plot(f, -(coefvector(1)*sqrt(f)+coefvector(2)*f+coefvector(3)./sqrt(f)))
endfunction
