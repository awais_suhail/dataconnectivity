function yd = semilogxinterp1(x, y, freq)
%SEMILOGXINTERP1 Summary of this function goes here
%   Detailed explanation goes here
yd = [];

idx1 = 1;
[~,idx2] = min(abs(freq-x(1)));

yd = nan(1, idx2-idx1);

for k = 1:numel(x)-1
    idx1 = idx2;
    [~,idx2] = min(abs(freq-x(k+1)));

    nel = idx2 - idx1;
    if y(k+1) == y(k)
        yd = [yd, y(k) .* ones(1, nel)];
    else
        xd = (linspace(x(k), x(k+1), nel+1));
        xd = xd(1:end-1);
        yd = [yd, y(k) + (y(k+1)-y(k))*((log(xd)-log(x(k)))./(log(x(k+1))-log(x(k))))];
    end
end

idx1 = idx2;
idx2 = numel(freq);
yd = [yd, y(end) nan(1, idx2-idx1)];

end

