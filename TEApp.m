function varargout = TEApp(varargin)

% TEAPP MATLAB code for TEApp.fig
%      TEAPP, by itself, creates a new TEAPP or raises the existing
%      singleton*.
%
%      H = TEAPP returns the handle to a new TEAPP or the handle to
%      the existing singleton*.
%
%      TEAPP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEAPP.M with the given input arguments.
%
%      TEAPP('Property','Value',...) creates a new TEAPP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TEApp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TEApp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TEApp

% Last Modified by GUIDE v2.5 12-Dec-2019 16:49:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TEApp_OpeningFcn, ...
                   'gui_OutputFcn',  @TEApp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

global connector_db;
connector_db = loadConnectorDB('coax');

global header_index;
global inline_index;
header_index = [];
inline_index = [];
connectors = fieldnames(connector_db);

for k = 1:numel(connectors)
    connectornames{k,1} = connector_db.(connectors{k}).name;
    if strcmp(connector_db.(connectors{k}).type, 'header')
        header_index = [header_index, k];
    elseif strcmp(connector_db.(connectors{k}).type, 'inline')
        inline_index = [inline_index, k];
    end
end

global CustomerVersion;
CustomerVersion = 'BMW';
%CustomerVersion = '';

global maxLimits;
maxLimits.fmin = 0;
maxLimits.fmax = 9;
if strcmp(CustomerVersion, 'BMW')
    maxLimits.fmax = 6;
end



% --- Executes just before TEApp is made visible.
function TEApp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TEApp (see VARARGIN)

% Choose default command line output for TEApp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CustomerVersion;
if strcmp(CustomerVersion, 'BMW')
    handles.btn_header_left.Visible = "off";
%    handles.btn_cable1.Visible = "off";
    handles.btn_inliner1.Visible = "off";
%    handles.btn_cable2.Visible = "off";
    handles.btn_inliner2.Visible = "off";
%    handles.btn_cable3.Visible = "off";
    handles.btn_inliner3.Visible = "off";
%    handles.btn_cable4.Visible = "off";
    handles.btn_inliner4.Visible = "off";
%    handles.btn_cable5.Visible = "off";
    handles.btn_header_right.Visible = "off";
%    handles.checkbox2.Visible = "off";
%    handles.checkbox3.Visible = "off";
%    handles.checkbox4.Visible = "off";
%    handles.checkbox5.Visible = "off";
%    handles.checkbox6.Visible = "off";
%    handles.checkbox7.Visible = "off";
%    handles.checkbox8.Visible = "off";
%    handles.checkbox9.Visible = "off";
%    handles.checkbox10.Visible = "off";
    % outputs
    handles.checkbox_S11.Visible = 'off';
    handles.checkbox_IRR.Visible = 'off';
    % limits
    handles.chkbox_TElimit.Visible = 'off';
    handles.chkbox_APIX3.Visible = 'off';
    handles.chkbox_FPD4.Visible = 'off';
    handles.chkbox_APIX3_IL.Visible = 'off';
    handles.chkbox_FPD4_IL.Visible = 'off';
    
    handles.chkbox_GMSL2_RL.String = 'SerDes';
    handles.chkbox_GMSL2_IL.String = 'SerDes';
    handles.chkbox_GMSL2_IRR.String = "SerDes";
    
    handles.checkbox_S21.String = 'IL range';
end

% Change path to application root to make it find files if deployed
if isdeployed % Stand-alone mode.
    [status, result] = system('path');
    currentDir = char(regexpi(result, 'Path=(.*?);', 'tokens', 'once'));
    cd(currentDir);
end

% This sets up the initial plot - only do when we are invisible
% so window can get raised using TEApp.
if strcmp(get(hObject,'Visible'),'off')
    a = plot([1 2 3 4 5]);
    delete(a);
end
plotted_lines=0;
assignin('base', 'plotted_lines', plotted_lines);

javaFrame = get(hObject,'JavaFrame');
javaFrame.setFigureIcon(javax.swing.ImageIcon('img/icon.png'));

try
    set(handles.txt_calculating,'Visible','off');

    axes(handles.axes1)
    matlabImage = imread('tec_lckp_rgb_orn.jpg');
    imshow(matlabImage)
    axes(handles.axes2)
    hold on;
    
%    temp = evalin('base','headerP1_noFiles');
%    set(handles.btn_header_left,'BackgroundColor','green');
    
%    temp = evalin('base','headerP2_noFiles');
%    set(handles.btn_header_right,'BackgroundColor','green');
    
%    temp=evalin('base','cable1_data'); 
%    set(handles.btn_cable1,'BackgroundColor','green');
    
    set(handles.pushbutton1,'Enable','on');
    set(handles.btn_inliner1,'Enable','on');
    set(handles.btn_cable2,'Enable','on');
    set(handles.popup_cable2,'Enable','on');
    set(handles.edit_cable2_length,'Enable','on');
    
    
%    temp = evalin('base','inliner1_noFiles');
%    set(handles.btn_inliner1,'BackgroundColor','green');
    
%    temp = evalin('base','cable2_data'); 
%    set(handles.btn_cable2,'BackgroundColor','green');
    
%    set(handles.btn_inliner2,'Enable','on');
%    set(handles.btn_cable3,'Enable','on');
%    set(handles.popup_cable3,'Enable','on');
%    set(handles.edit_cable3_length,'Enable','on');
    
    
%    temp = evalin('base','inliner2_noFiles');
%    temp = evalin('base','cable3_data');
    
%    set(handles.btn_inliner2,'BackgroundColor','green');
%    set(handles.btn_cable3,'BackgroundColor','green');
    
%    set(handles.btn_inliner3,'Enable','on');
%    set(handles.btn_cable4,'Enable','on');
%    set(handles.popup_cable4,'Enable','on');
%    set(handles.edit_cable4_length,'Enable','on');
    
    
%    temp = evalin('base','inliner3_noFiles');
%    temp = evalin('base','cable4_data');
    
%    set(handles.btn_inliner3,'BackgroundColor','green');
%    set(handles.btn_cable4,'BackgroundColor','green');
    
%    set(handles.btn_inliner4,'Enable','on');
%    set(handles.btn_cable5,'Enable','on');
%    set(handles.popup_cable5,'Enable','on');
%    set(handles.edit_cable5_length,'Enable','on');

    
%    temp = evalin('base','inliner4_noFiles');
%    temp = evalin('base','cable5_data');
    
%    set(handles.btn_inliner4,'BackgroundColor','green');
%    set(handles.btn_cable5,'BackgroundColor','green');
    
catch

end

bgcolor = get(gcf, 'Color');

labelStr = '<html><center><b>TE Connectivity</b> Confidential & Proprietary.<br />Do not reproduce or distribute.</center></html>';
jLabelDisclaimer = javaObjectEDT('javax.swing.JLabel',labelStr);
jLabelDisclaimer.setBackground(java.awt.Color(bgcolor(1),bgcolor(2),bgcolor(3)));
[hcomponent,hcontainer] = javacomponent(jLabelDisclaimer,[1030,25,231,30],gcf);

% labelStr = '<html><i>f</i><sub>min</sub> (GHz)</html>';
% jLabelFmin = javaObjectEDT('javax.swing.JLabel',labelStr);
% jLabelFmin.setBackground(java.awt.Color(bgcolor(1),bgcolor(2),bgcolor(3)));
% [hcomponent,hcontainer] = javacomponent(jLabelFmin,[70,90,70,20],gcf);


global cables;
global CableTable;
global connector_db;
global maxLimits;

handles.edit_fmax.String = maxLimits.fmax;

LoadCableData();
name2 = strcat(num2str(CableTable.uid), {' '}, CableTable.man, {' '}, CableTable.name, {' ('}, CableTable.type, {')'});

handles.popup_cable1.String = name2';
handles.popup_cable2.String = name2';
handles.popup_cable3.String = name2';
handles.popup_cable4.String = name2';
handles.popup_cable5.String = name2';

connectors = fieldnames(connector_db);
for k = 1:numel(connectors)
    connectornames{k,1} = connector_db.(connectors{k}).name;
end
global header_index;
global inline_index;

handles.pop_header_l.String = connectornames(header_index);
handles.pop_header_r.String = connectornames(header_index);
handles.pop_inline_1.String = connectornames(inline_index);
handles.pop_inline_2.String = connectornames(inline_index);
handles.pop_inline_3.String = connectornames(inline_index);
handles.pop_inline_4.String = connectornames(inline_index);

% name1 = fieldnames(cables);
% for k = 1:numel(name1)
%     currentcables = fieldnames(cables.(name1{k}).cables);
%     for l = 1:numel(currentcables)
%         cables.(name1{k}).cables.(currentcables{1})
%     end
% end

% UIWAIT makes TEApp wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TEApp_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CustomerVersion;
data = guidata(hObject);

handles.check_envelope.Value = 0;
handles.check_irr_env.Value = 0;
if isfield(data, 'p_s11_max');
    hold off;
    delete(data.p_s11_max);
    hold on;
    guidata(hObject,data);
end
if isfield(data, 'p_irr_env');
    hold off;
    delete(data.p_irr_env);
    hold on;
    guidata(hObject,data);
end

tic;

%Show that calculation is running
set(handles.txt_calculating,'Visible','on');
drawnow;

%headerP1=evalin('base','headerP1_data1');
%freq=headerP1.Frequencies;
global freq;
freq=linspace(10e6, 10e9, 4001)';
o=1;

% Set cable data

% Calculate cable impedance steps
for cable_segment = 1:5
    pos = sprintf('edit_cable%d_steps', cable_segment);
    impmin = sprintf('edit_cable%d_impmin', cable_segment);
    impmax = sprintf('edit_cable%d_impmax', cable_segment);
    if str2double(get(handles.(pos),'String'))==1
        if str2double(get(handles.(impmin),'String')) == str2double(get(handles.(impmax),'String'))
            cable_imp_range{cable_segment}=str2double(get(handles.(impmin),'String'));
        else
            cable_imp_range{cable_segment}=mean([str2double(get(handles.(impmin),'String')),str2double(get(handles.(impmax),'String'))]);
        end
    else
        cable_imp_range{cable_segment} = linspace( ...
            str2double(get(handles.(impmin),'String')), ...
            str2double(get(handles.(impmax),'String')), ...
            str2double(get(handles.(pos),'String')) ...
            );
    end

    global CableTable;
    cable_para_string = ['popup_cable' num2str(cable_segment)];
    cableNumber = handles.(cable_para_string).Value;
    cable_parameters = CableTable.parameters{cableNumber};

    for i=1:length(cable_imp_range{cable_segment})
        CabParam.Zw=cable_imp_range{cable_segment}(i);
        %%CabParam.c1=-2.5898e-05;
        CabParam.c1=cable_parameters.c1; %RTK031
        %CabParam.c1=-2.7e-05; %STP
        CabParam.c2=cable_parameters.c2; %RTK031
        %CabParam.c2=-15.0924e-11; %STP
        CabParam.vp=2.16e8; %RTK031
        CabParam.Z0=50;
        CabParam.THA_mul=1;
        CabParam.cab_mul=1;
        
        CabParam.len=str2num(get(handles.(['edit_cable' num2str(cable_segment) '_length']),'String'));

        cable_data{i} = GetCabParameter(CabParam, freq);

        %cable_data(i) = abcdparameters(cable_data_temp,freq);
    end
    assignin('base', ['cable' num2str(cable_segment) '_data'], cable_data)
    clear cable_data;
end

% general channel build-up
channel_model={'headerP1', 'cable1', 'inliner1', 'cable2', 'inliner2', ...
    'cable3', 'inliner3', 'cable4', 'inliner4', 'cable5', 'inliner5', ...
    'cable6', 'headerP2'};

% get actual channel information from the variables available in base ws
base_workspace = evalin('base','whos');

% save header data as first result
if handles.btn_headerP1.BackgroundColor == [0 1 0] & ismember([channel_model{1} '_data'],{base_workspace(:).name})
    result_tree = evalin('base',[channel_model{1} '_data']);
else
    S11=zeros(1,length(freq));
    S12=ones(1,length(freq));
    S21=ones(1,length(freq));
    S22=zeros(1,length(freq));

    s_params(1,1,:) = S11;
    s_params(2,1,:) = S21;
    s_params(1,2,:) = S12;
    s_params(2,2,:) = S22;

    result_tree{1} = abcdparameters(s2abcd(s_params,50),freq);
end
channel_model(1)=[];

% iterate over channel
while not(isempty(channel_model))
    fprintf([channel_model{1} ' ']);
    
    % first concatenate cable
    btn = ['btn_' channel_model{1}];
    if ~isempty(strfind(channel_model{1}, 'inliner'))
        btn = [btn '_toggle'];
    end

    if ismember([channel_model{1} '_data'],{base_workspace(:).name}) & handles.(btn).BackgroundColor == [0 1 0]
        count = 1;

        segment_data_temp=evalin('base',[channel_model{1} '_data']);
        for na=1:length(segment_data_temp)
            for nb=1:length(result_tree)
                result_tree_new{count} = casc_TS_single(freq, result_tree{nb}, segment_data_temp{na});
                count=count+1;
                if strcmp(CustomerVersion, '')
                    fprintf('.');
                end
            end
        end

    elseif ismember([channel_model{1} '_data1'],{base_workspace(:).name}) & handles.(btn).BackgroundColor == [0 1 0]
        count = 1;

        for na=1:evalin('base', [channel_model{1} '_noFiles'])
            segment_data_temp=evalin('base',[channel_model{1} '_data' num2str(na)]);
            for nb=1:length(result_tree)
                result_tree_new{count}=casc_TS_single(freq, result_tree{nb}, segment_data_temp);
                count=count+1;
                if strcmp(CustomerVersion, '')
                    fprintf('.');
                end
            end
        end
    else
        result_tree_new={};
    end

    if not(isempty(result_tree_new))
        result_tree=result_tree_new;
        result_tree_new={};
    end

    fprintf(' done\n');
    channel_model(1)=[];
end

calc_time=toc;
if strcmp(CustomerVersion, '')
    fprintf([num2str(length(result_tree)) ' variations calculated in ' num2str(calc_time) ' seconds!\n'])
end

for k=1:length(result_tree)
    temp=sparameters(result_tree{k});
    results11(:,k)=rfparam(temp,1,1);
    results22(:,k)=rfparam(temp,2,2);
    results21(:,k)=rfparam(temp,2,1);
end

global Results;
Results.s11 = results11;
Results.s22 = results22;
Results.s21 = results21;

set(handles.axes1,'defaultLineLineWidth',2);
hold on

data=guidata(hObject);

% calculate s11
if strcmp(CustomerVersion, '')
    data.p_s11_a = plot(freq, 20*log10(abs(results11(:,1:end))));
    data.p_s11_b = plot(freq, 20*log10(abs(results22(:,1:end))));    

    if get(handles.checkbox_S11,'Value')==0
        set(data.p_s11_a,'visible','off');
        set(data.p_s11_b,'visible','off');
    end
end

% calculate s21
if strcmp(CustomerVersion, '')
    data.p_s21 = plot(freq, 20*log10(abs(results21(:,1:end))));
else
    s21_sort = sort(results21(:,1:end), 2, 'ComparisonMethod', 'abs');
    s21_max = abs(s21_sort(:,end));
    s21_min = abs(s21_sort(:,1));
    col = [1, 0.5, 0];
    data.p_s21 = fill([freq; flip(freq)], [20*log10(s21_min(:,1:end)); flip(20*log10(s21_max(:,1:end)))], col, 'EdgeColor', col);
end

if get(handles.checkbox_S21,'Value')==0
    set(data.p_s21,'visible','off');
end

% calculate IRR
if strcmp(CustomerVersion, '')

    s11_dB=20*log10(abs(results11(:,1:end)));

    ws = warning('off','all');  % Turn off warning of polyfit
    for h=1:length(s11_dB(1,:))
        p1(:,h)=polyfit(freq,s11_dB(:,h),20);
        s11_fit_dB(:,h)=polyval(p1(:,h),freq);
        
        s22_dB=20*log10(abs(results22(:,1:end)));
        p2(:,h)=polyfit(freq,s22_dB(:,h),20);
        s22_fit_dB(:,h)=polyval(p2(:,h),freq);
        
        s21_dB=20*log10(abs(results21(:,1:end)));
        
        IRR_21_fit(:,h)=s21_dB(:,h)-s11_fit_dB(:,h);
        IRR_12_fit(:,h)=s21_dB(:,h)-s22_fit_dB(:,h);
    end
    warning(ws) % Turn warning back on.
    
    Results.irr21 = IRR_21_fit;
    Results.irr12 = IRR_12_fit;
    
    data.p_irr_a = plot(freq, IRR_21_fit(:,1:end));
    data.p_irr_b = plot(freq, IRR_12_fit(:,1:end));
    
    if get(handles.checkbox_IRR,'Value')==0
        set(data.p_irr_a,'visible','off');
        set(data.p_irr_b,'visible','off');
    end
end

legend(handles.axes2, 'off');
axis([str2num(get(handles.edit_fmin,'String'))*1e9 str2num(get(handles.edit_fmax,'String'))*1e9 str2num(get(handles.edit_dBmin,'String')) str2num(get(handles.edit_dBmax,'String'))]);
set(gca,'FontSize',str2num(get(handles.edit_axis_size,'String')));
grid on;
xlabel('Frequency in Hz')
ylabel('S-Parameters in dB')
disp('done')

%Show that calculation is done
set(handles.txt_calculating,'Visible','off');
handles.checkbox_S11.Enable = 'on';
handles.checkbox_S21.Enable = 'on';
handles.checkbox_IRR.Enable = 'on';

if size(results11, 2) > 8
    handles.check_envelope.Enable = 'on';
    handles.check_irr_env.Enable = 'on';
end

guidata(hObject,data);



% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on button press in btn_header_left.
function btn_header_left_Callback(hObject, eventdata, handles)
% hObject    handle to btn_header_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Option#1: load single file 
% [filename, filepath] = uigetfile({'*.s2p'});
% try
%     headerP1_data=abcdparameters([filepath filename]);
%     assignin('base', 'headerP1_data', headerP1_data);
%     set(handles.btn_header_left,'BackgroundColor','green');
%     set(handles.txt_header_left,'Visible','on');
%     set(handles.txt_header_left,'String',filename);    
% catch
%     set(handles.text1, 'String', 'Error with file import'); 
% end

%Option#2: load all files within a folder (tolerance study)
try
    path = uigetdir;
    liste = dir(path);
    files = {liste.name};
    headerP1_data = {};
    for k=3:numel(files) 
        % read files
        headerP1_data{k-2} = abcdparameters([path '\' files{k}]);
    end 
    assignin('base', 'headerP1_data', headerP1_data)
    set(handles.btn_header_left,'BackgroundColor','green');
    set(handles.txt_header_left,'Visible','on');
    set(handles.txt_header_left,'String',[num2str(k-2) ' files loaded']);
    assignin('base', 'headerP1_noFiles', k-2)
catch
    set(handles.text1, 'String', 'Error with file import'); 
end

try
    temp=evalin('base','cable1_data');
    temp=evalin('base','headerP2_noFiles');
    set(handles.pushbutton1,'Enable','on');
    set(handles.btn_inliner1,'Enable','on');
    set(handles.btn_cable2,'Enable','on');
    set(handles.popup_cable2,'Enable','on');
    set(handles.edit_cable2_length,'Enable','on');
catch
end

% --- Executes on button press in btn_cable1.
function btn_cable1_Callback(hObject, eventdata, handles)
% hObject    handle to btn_cable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.btn_cable1.BackgroundColor == [1 0 0]
    set(handles.btn_cable1,'BackgroundColor','green');
else
    set(handles.btn_cable1,'BackgroundColor','red');
end

 

% --- Executes on button press in btn_inliner1.
function btn_inliner1_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Option#1: load single file 
% [filename, filepath] = uigetfile({'*.s2p'});
% try
%     inlinerNO1_data=abcdparameters([filepath filename]);
%     assignin('base', 'inlinerNO1_data', inlinerNO1_data)
%     set(handles.btn_inliner1,'BackgroundColor','green');
% catch
%     set(handles.text1, 'String', 'Error with file import'); 
% end

%Option#2: load all files within a folder (tolerance study)
try
    path = uigetdir;
    liste = dir(path);
    files = {liste.name};
    inliner1_data = {};
    for k=3:numel(files) 
        % read files
        inliner1_data{k-2} = abcdparameters([path '\' files{k}]);
    end
    assignin('base', 'inliner1_data', inliner1_data)
    set(handles.btn_inliner1,'BackgroundColor','green');
    assignin('base', 'inliner1_noFiles', k-2)
catch
    set(handles.text1, 'String', 'Error with file import'); 
end

try
        temp=evalin('base','cable2_data');
        set(handles.btn_inliner2,'Enable','on');
        set(handles.btn_cable3,'Enable','on'); 
        set(handles.popup_cable3,'Enable','on'); 
        set(handles.edit_cable3_length,'Enable','on');      
catch
end



% --- Executes on button press in btn_cable2.
function btn_cable2_Callback(hObject, eventdata, handles)
% hObject    handle to btn_cable2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_cable2.BackgroundColor == [1 0 0]
    set(handles.btn_cable2,'BackgroundColor','green');
else
    set(handles.btn_cable2,'BackgroundColor','red');
end



% --- Executes on button press in btn_inliner2.
function btn_inliner2_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Option#1: load single file 
% [filename, filepath] = uigetfile({'*.s2p'});
% try
%     inlinerNO2_data=abcdparameters([filepath filename]);
%     assignin('base', 'inlinerNO2_data', inlinerNO2_data)
%     set(handles.btn_inliner2,'BackgroundColor','green');
% catch
%     set(handles.text1, 'String', 'Error with file import'); 
% end

%Option#2: load all files within a folder (tolerance study)
try
    path = uigetdir
    liste = dir(path);
    files = {liste.name};
    inliner2_data = {};
    for k=3:numel(files) 
        % read files
        inliner2_data{k-2} = abcdparameters([path '\' files{k}]);
    end
    assignin('base', 'inliner2_data', inliner2_data)
    set(handles.btn_inliner2,'BackgroundColor','green');
    assignin('base', 'inliner2_noFiles', k-2)
catch
    set(handles.text1, 'String', 'Error with file import'); 
end

try
        temp=evalin('base','cable3_data');
        set(handles.btn_inliner3,'Enable','on');
        set(handles.btn_cable4,'Enable','on'); 
        set(handles.popup_cable4,'Enable','on'); 
        set(handles.edit_cable4_length,'Enable','on');      
catch
end


% --- Executes on button press in btn_cable3.
function btn_cable3_Callback(hObject, eventdata, handles)
% hObject    handle to btn_cable3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_cable3.BackgroundColor == [1 0 0]
    set(handles.btn_cable3,'BackgroundColor','green');
else
    set(handles.btn_cable3,'BackgroundColor','red');
end



% --- Executes on button press in btn_inliner3.
function btn_inliner3_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Option#1: load single file 
%[filename, filepath] = uigetfile({'*.s2p'});
%try
%    inlinerNO3_data=abcdparameters([filepath filename]);
%    assignin('base', 'inlinerNO3_data', inlinerNO3_data)
%    set(handles.btn_inliner3,'BackgroundColor','green');
%catch
%    set(handles.text1, 'String', 'Error with file import'); 
%end
%------------------------------------------------------------------

%Option#2: load all files within a folder (tolerance study)
try
    path = uigetdir;
    liste = dir(path);
    files = {liste.name};
    inliner3_data = {};
    for k=3:numel(files) 
        % read files
        inliner3_data{k-2} = abcdparameters([path '\' files{k}]);
    end
    assignin('base', 'inliner3_data', inliner3_data)
    set(handles.btn_inliner3,'BackgroundColor','green');
    assignin('base', 'inliner3_noFiles', k-2)
catch
    set(handles.text1, 'String', 'Error with file import'); 
end
try
        temp=evalin('base','cable3_data');
        set(handles.btn_inliner4,'Enable','on');
        set(handles.btn_cable5,'Enable','on'); 
        set(handles.popup_cable5,'Enable','on'); 
        set(handles.edit_cable5_length,'Enable','on');
catch
end



% --- Executes on button press in btn_cable4.
function btn_cable4_Callback(hObject, eventdata, handles)
% hObject    handle to btn_cable4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_cable4.BackgroundColor == [1 0 0]
    set(handles.btn_cable4,'BackgroundColor','green');
else
    set(handles.btn_cable4,'BackgroundColor','red');
end



% --- Executes on button press in btn_inliner4.
function btn_inliner4_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Option#1: load single file 
% [filename, filepath] = uigetfile({'*.s2p'});
% try
%     inlinerNO4_data=abcdparameters([filepath filename]);
%     assignin('base', 'inlinerNO4_data', inlinerNO4_data)
%     set(handles.btn_inliner4,'BackgroundColor','green');
% catch
%     set(handles.text1, 'String', 'Error with file import'); 
% end
%----------------------------------------------------------------

%Option#2: load all files within a folder (tolerance study)
try
    path = uigetdir
    liste = dir(path);
    files = {liste.name};
    inliner4_data = {};
    for k=3:numel(files) 
        % read files
        inliner4_data{k-2} = abcdparameters([path '\' files{k}]);
    end
    assignin('base', 'inliner4_data', inliner4_data)
    set(handles.btn_inliner4,'BackgroundColor','green');
    assignin('base', 'inliner4_noFiles', k-2)
catch
    set(handles.text1, 'String', 'Error with file import'); 
end


% --- Executes on button press in btn_cable5.
function btn_cable5_Callback(hObject, eventdata, handles)
% hObject    handle to btn_cable5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_cable5.BackgroundColor == [1 0 0]
    set(handles.btn_cable5,'BackgroundColor','green');
else
    set(handles.btn_cable5,'BackgroundColor','red');
end



% --- Executes on button press in btn_header_right.
function btn_header_right_Callback(hObject, eventdata, handles)
% hObject    handle to btn_header_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Option#1: load single file 
% [filename, filepath] = uigetfile({'*.s2p'});
% try
%     headerP2_data=sparameters([filepath filename]);
%     freq=headerP2_data.Frequencies;
%     s2p_new = snp2smp(headerP2_data,[2 1]);
%     abcd_new=s2abcd(s2p_new.Parameters, s2p_new.Impedance);
%     headerP2_data=abcdparameters(abcd_new, freq);
%     
%     assignin('base', 'headerP2_data', headerP2_data)
%     set(handles.btn_header_right,'BackgroundColor','green');
%     
%     set(handles.txt_header_right,'Visible','on');
%     set(handles.txt_header_right,'String',filename);
% catch
%     set(handles.text1, 'String', 'Error with file import'); 
% end 

%Option#2: load all files within a folder (tolerance study)
try
    path = uigetdir
    liste = dir(path);
    files = {liste.name};
    headerP2_data = {};
    for k=3:numel(files) 
        % read files
        headerP2_data{k-2} = sparameters([path '\' files{k}]);
        freq = headerP2_data{k-2}.Frequencies;
        s2p_new = snp2smp(headerP2_data{k-2}, [2 1]);
        abcd_new = s2abcd(s2p_new.Parameters, s2p_new.Impedance);
        headerP2_data{k-2} = abcdparameters(abcd_new, freq);
    end 
    assignin('base', 'headerP2_data', headerP2_data)
    set(handles.btn_header_right,'BackgroundColor','green');
    set(handles.txt_header_right,'Visible','on');
    set(handles.txt_header_right,'String',[num2str(k-2) ' files loaded']);
    assignin('base', 'headerP2_noFiles', k-2)
catch
    set(handles.text1, 'String', 'Error with file import'); 
end

try
    temp=evalin('base','cable1_data');
    temp=evalin('base','headerP1_noFiles');
    set(handles.pushbutton1,'Enable','on');
    set(handles.btn_inliner1,'Enable','on');
    set(handles.btn_cable2,'Enable','on'); 
    set(handles.popup_cable2,'Enable','on'); 
    set(handles.edit_cable2_length,'Enable','on'); 
catch
end



% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7


% --- Executes on button press in checkbox8.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox8


% --- Executes on button press in checkbox9.
function checkbox9_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox9


% --- Executes on button press in checkbox10.
function checkbox10_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox10


% --- Executes on selection change in popup_cable1.
function popup_cable1_Callback(hObject, eventdata, handles)
% hObject    handle to popup_cable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%disp(['Cable ' num2str(handles.popup_cable1.Value) ' selected.']);

setCable(handles.popup_cable1.Value, handles, 1)


% Hints: contents = cellstr(get(hObject,'String')) returns popup_cable1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_cable1


% --- Executes during object creation, after setting all properties.
function popup_cable1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_cable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_cable2.
function popup_cable2_Callback(hObject, eventdata, handles)
% hObject    handle to popup_cable2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_cable2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_cable2
setCable(handles.popup_cable2.Value, handles, 2)


% --- Executes during object creation, after setting all properties.
function popup_cable2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_cable2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_cable3.
function popup_cable3_Callback(hObject, eventdata, handles)
% hObject    handle to popup_cable3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_cable3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_cable3
setCable(handles.popup_cable3.Value, handles, 3)


% --- Executes during object creation, after setting all properties.
function popup_cable3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_cable3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_cable4.
function popup_cable4_Callback(hObject, eventdata, handles)
% hObject    handle to popup_cable4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_cable4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_cable4
setCable(handles.popup_cable4.Value, handles, 4)


% --- Executes during object creation, after setting all properties.
function popup_cable4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_cable4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_cable5.
function popup_cable5_Callback(hObject, eventdata, handles)
% hObject    handle to popup_cable5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_cable5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_cable5
setCable(handles.popup_cable5.Value, handles, 5)


% --- Executes during object creation, after setting all properties.
function popup_cable5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_cable5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable1_length_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable1_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable1_length as text
%        str2double(get(hObject,'String')) returns contents of edit_cable1_length as a double


% --- Executes during object creation, after setting all properties.
function edit_cable1_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable1_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable2_length_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable2_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable2_length as text
%        str2double(get(hObject,'String')) returns contents of edit_cable2_length as a double


% --- Executes during object creation, after setting all properties.
function edit_cable2_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable2_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable3_length_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable3_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable3_length as text
%        str2double(get(hObject,'String')) returns contents of edit_cable3_length as a double


% --- Executes during object creation, after setting all properties.
function edit_cable3_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable3_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable4_length_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable4_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable4_length as text
%        str2double(get(hObject,'String')) returns contents of edit_cable4_length as a double


% --- Executes during object creation, after setting all properties.
function edit_cable4_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable4_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable5_length_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable5_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable5_length as text
%        str2double(get(hObject,'String')) returns contents of edit_cable5_length as a double


% --- Executes during object creation, after setting all properties.
function edit_cable5_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable5_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_fmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_fmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_fmin as text
%        str2double(get(hObject,'String')) returns contents of edit_fmin as a double
global maxLimits;

ax = gca;

if str2double(get(hObject,'String')) < maxLimits.fmin
    hObject.String = num2str(maxLimits.fmin);
end

if str2double(get(hObject,'String')) < str2double(handles.edit_fmax.String)
    set(hObject,'BackgroundColor','white');
    handles.edit_fmax.BackgroundColor = 'white';
    ax.XLim = [str2double(get(hObject,'String')).*1e9 str2double(handles.edit_fmax.String).*1e9];
else
    set(hObject,'BackgroundColor','red');
end



% --- Executes during object creation, after setting all properties.
function edit_fmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_fmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_fmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_fmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_fmax as text
%        str2double(get(hObject,'String')) returns contents of edit_fmax as a double
global maxLimits;

ax = gca;

if str2double(get(hObject,'String')) > maxLimits.fmax
    hObject.String = num2str(maxLimits.fmax);
end

if str2double(get(hObject,'String')) > str2double(handles.edit_fmin.String)
    set(hObject,'BackgroundColor','white');
    handles.edit_fmin.BackgroundColor = 'white';
    ax.XLim = [str2double(handles.edit_fmin.String).*1e9 str2double(get(hObject,'String')).*1e9];
else
    set(hObject,'BackgroundColor','red');
end



% --- Executes during object creation, after setting all properties.
function edit_fmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_fmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_dBmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dBmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dBmin as text
%        str2double(get(hObject,'String')) returns contents of edit_dBmin as a double
ax = gca;
if str2double(get(hObject,'String')) < str2double(handles.edit_dBmax.String)
    set(hObject,'BackgroundColor','white');
    handles.edit_dBmax.BackgroundColor = 'white';
    ax.YLim = [str2double(get(hObject,'String')) str2double(handles.edit_dBmax.String)];
else
    set(hObject,'BackgroundColor','red');
end


% --- Executes during object creation, after setting all properties.
function edit_dBmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dBmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_dBmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dBmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dBmax as text
%        str2double(get(hObject,'String')) returns contents of edit_dBmax as a double
ax = gca;
if str2double(get(hObject,'String')) > str2double(handles.edit_dBmin.String)
    set(hObject,'BackgroundColor','white');
    handles.edit_dBmin.BackgroundColor = 'white';
    ax.YLim = [str2double(handles.edit_dBmin.String) str2double(get(hObject,'String'))];
else
    set(hObject,'BackgroundColor','red');
end



% --- Executes during object creation, after setting all properties.
function edit_dBmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dBmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_axis_size_Callback(hObject, eventdata, handles)
% hObject    handle to edit_axis_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_axis_size as text
%        str2double(get(hObject,'String')) returns contents of edit_axis_size as a double


% --- Executes during object creation, after setting all properties.
function edit_axis_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_axis_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in btn_clear.
function btn_clear_Callback(hObject, eventdata, handles)
% hObject    handle to btn_clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hold off
cla(handles.axes2)
delete(handles.axes2.Legend)
handles.checkbox_S11.Enable = 'off';
handles.checkbox_S21.Enable = 'off';
handles.checkbox_IRR.Enable = 'off';

handles.chkbox_TElimit.Value = 0;
handles.chkbox_NGlimit.Value = 0;
handles.chkbox_APIX3.Value = 0;
handles.chkbox_GMSL2_RL.Value = 0;
handles.chkbox_FPD4.Value = 0;
handles.chkbox_ASA_RL.Value = 0;
handles.chkbox_APIX3_IL.Value = 0;
handles.chkbox_GMSL2_IL.Value = 0;
handles.chkbox_FPD4_IL.Value = 0;
handles.chkbox_GMSL2_IRR.Value = 0;
handles.chkbox_ASA_IL.Value = 0;
handles.chkbox_NGAUTO_IL.Value = 0;

handles.check_envelope.Value = 0;
handles.check_envelope.Enable = 'Off';
%handles.slider_envelope.Visible = 'Off';
handles.envelope_type.Visible = 'Off';
handles.check_irr_env.Value = 0;
handles.check_irr_env.Enable = 'Off';
%handles.slider_IRR_env.Visible = 'Off';
handles.IRR_env_type.Visible = 'Off';

global Results;
clear('Results.s11_max');

hold on;



function edit_cable1_impmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable1_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable1_impmin as text
%        str2double(get(hObject,'String')) returns contents of edit_cable1_impmin as a double


% --- Executes during object creation, after setting all properties.
function edit_cable1_impmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable1_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable5_imp_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable5_imp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable5_imp as text
%        str2double(get(hObject,'String')) returns contents of edit_cable5_imp as a double


% --- Executes during object creation, after setting all properties.
function edit_cable5_imp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable5_imp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable4_imp_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable4_imp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable4_imp as text
%        str2double(get(hObject,'String')) returns contents of edit_cable4_imp as a double


% --- Executes during object creation, after setting all properties.
function edit_cable4_imp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable4_imp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable1_steps_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable1_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable1_steps as text
%        str2double(get(hObject,'String')) returns contents of edit_cable1_steps as a double


% --- Executes during object creation, after setting all properties.
function edit_cable1_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable1_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable1_impmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable1_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable1_impmax as text
%        str2double(get(hObject,'String')) returns contents of edit_cable1_impmax as a double


% --- Executes during object creation, after setting all properties.
function edit_cable1_impmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable1_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkbox_TElimit.
function chkbox_TElimit_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_TElimit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_TElimit

data=guidata(hObject);
global freq;

x = [  0   1   2 2.5 3.5   5   6   7   8] * 1e9;
y = [-25 -25 -22 -20 -17 -10 -10 -10 -10];
yd = interp1(x, y, freq);


if get(handles.chkbox_TElimit,'Value')==1
    data.TELimitPlot = plot(freq, yd, '--r');
else
    delete(data.TELimitPlot);
end

guidata(hObject,data);


% --- Executes on button press in chkbox_NGlimit.
function chkbox_NGlimit_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_NGlimit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_NGlimit
data=guidata(hObject);
global freq;

x = [1e-3 4.8e-1   3   4] * 1e9;
y = [ -20    -20 -12 -12];
yd = semilogxinterp1(x, y, freq);

if get(handles.chkbox_NGlimit,'Value')==1
     data.NGLimitPlot = plot(freq, yd, '--r');
else
    delete(data.NGLimitPlot);
end

guidata(hObject,data);

% --- Executes on button press in checkbox_S11.
function checkbox_S11_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_S11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_S11
data=guidata(hObject);

if get(handles.checkbox_S11,'Value')==0
    set(data.p_s11_a,'visible','off');
    set(data.p_s11_b,'visible','off');
else
    set(data.p_s11_a,'visible','on');
    set(data.p_s11_b,'visible','on');
end

guidata(hObject,data);



% --- Executes on button press in checkbox_S21.
function checkbox_S21_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_S21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_S21
data=guidata(hObject);

if get(handles.checkbox_S21,'Value')==0
    set(data.p_s21,'visible','off');
else
    set(data.p_s21,'visible','on');
end

guidata(hObject,data);



% --- Executes on button press in checkbox_IRR.
function checkbox_IRR_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_IRR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_IRR
data=guidata(hObject);

if get(handles.checkbox_IRR,'Value')==0
    set(data.p_irr_a,'visible','off');
    set(data.p_irr_b,'visible','off');
else
    set(data.p_irr_a,'visible','on');
    set(data.p_irr_b,'visible','on');
end

guidata(hObject,data);



% --- Executes on button press in chkbox_APIX3.
function chkbox_APIX3_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_APIX3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_APIX3

data=guidata(hObject);
global freq;

x = [0.1 0.4 1.5 2.25 4.5] * 1e9;
y = [-20 -20 -12  -8  -7];

yd = semilogxinterp1(x, y, freq);

if get(handles.chkbox_APIX3,'Value')==1
     data.APIXLimitPlot = plot(freq, yd, '--r');
else
    delete(data.APIXLimitPlot);
end

guidata(hObject,data);

% --- Executes on button press in chkbox_GMSL2_RL.
function chkbox_GMSL2_RL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_GMSL2_RL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_GMSL2_RL
data=guidata(hObject);
global freq;

global CustomerVersion;
if strcmp(CustomerVersion, 'BMW')
    [~,idx1] = min(abs(freq-1e6));
    [~,idx2] = min(abs(freq-10e6));
    [~,idx3] = min(abs(freq-1e9));
    [~,idx4] = min(abs(freq-3.25e9));
    
    yd = nan(1,numel(freq));
    yd(idx1:idx2) = -6-(0.9*freq(idx1:idx2)./1e6);
    yd(idx2:idx3) = interp1([10e6 1e9], [-15 -25], freq(idx2:idx3));
    yd(idx3:idx4) = interp1([1e9 3.25e9], [-25 -18], freq(idx3:idx4)); % -(21.2+0.28*sqrt(freq(idx3:idx4)./1e6)-6.2*freq(idx3:idx4)./1e9);
else
    [~,idx1] = min(abs(freq-1e6));
    [~,idx2] = min(abs(freq-10e6));
    [~,idx3] = min(abs(freq-1e9));
    [~,idx4] = min(abs(freq-3.5e9));
    
    yd = nan(1,numel(freq));
    yd(idx1:idx2) = -6-(0.9*freq(idx1:idx2)./1e6);
    yd(idx2:idx3) = -(14.2+0.28*sqrt(freq(idx2:idx3)./1e6)+0.8*freq(idx2:idx3)./1e9);
    yd(idx3:idx4) = -(21.2+0.28*sqrt(freq(idx3:idx4)./1e6)-6.2*freq(idx3:idx4)./1e9);
end

if get(handles.chkbox_GMSL2_RL,'Value')==1
    data.GMSLLimitPlot = plot(freq, yd, '--r');
else
    delete(data.GMSLLimitPlot);
end

guidata(hObject,data);



% --- Executes on button press in chkbox_GMSL2_IRR.
function chkbox_GMSL2_IRR_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_GMSL2_IRR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_GMSL2_IRR
data=guidata(hObject);
global freq;

global CustomerVersion;
if strcmp(CustomerVersion, 'BMW')
    [~,idx0] = min(abs(freq-10e6));
    [~,idx1] = min(abs(freq-3.25e9));
    IL_limit = NaN(1,length(freq));
    
    IL_limit(idx0:idx1) = interp1([10e6 3.25e9], [20 -5], freq(idx0:idx1));
else
    [~,idx0] = min(abs(freq-1e6));
    [~,idx1] = min(abs(freq-10e6));
    [~,idx2] = min(abs(freq-1e9));
    [~,idx3] = min(abs(freq-3.5e9));
    IL_limit = NaN(1,length(freq));
    
    IL_limit(idx0:idx1) = 5+.9.*freq(idx0:idx1)*1e6;
    IL_limit(idx1:idx2) = 14;
    IL_limit(idx2:idx3) = -7.*freq(idx2:idx3)./1e9+21;
end
if get(handles.chkbox_GMSL2_IRR,'Value')==1
    data.GMSLIRRLimitPlot = plot(freq, IL_limit, '--r');
else
    delete(data.GMSLIRRLimitPlot);
end

guidata(hObject,data);



% --- Executes on button press in chkbox_FPD4.
function chkbox_FPD4_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_FPD4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_FPD4
data=guidata(hObject);
global freq;

[~,idx1] = min(abs(freq-1e6));
[~,idx2] = min(abs(freq-100e6));
[~,idx3] = min(abs(freq-1e9));
[~,idx4] = min(abs(freq-4e9));

yd = nan(1,numel(freq));
yd(idx1:idx2) = -16*ones(1,idx2-idx1+1);
yd(idx2:idx3) = -9+7.*log10(freq(idx2:idx3)./1e9);
yd(idx3:idx4) = -9*ones(1,idx4-idx3+1);

if get(handles.chkbox_FPD4,'Value')==1
    data.FPDLimitPlot = plot(freq, yd,'--r');
else
    delete(data.FPDLimitPlot);
end

guidata(hObject,data);


% --- Executes on button press in chkbox_APIX3_IL.
function chkbox_APIX3_IL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_APIX3_IL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_APIX3_IL
data=guidata(hObject);
global freq;

if get(handles.chkbox_APIX3_IL,'Value')==1
    f=[100e6 1.5e9 2.25e9 3e9 4.5e9];
    apix3_IL=[-4 -9.2 -11.8 -14.5 -20];
    yd = interp1(f, apix3_IL, freq);
    data.APIXILLimitPlot = plot(freq, yd,'--r');
else
    delete(data.APIXILLimitPlot);
end

guidata(hObject,data);



% --- Executes on button press in chkbox_GMSL2_IL.
function chkbox_GMSL2_IL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_GMSL2_IL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_GMSL2_IL
data=guidata(hObject);
global freq;

[~,idx0] = min(abs(freq-1e6));
[~,idx1] = min(abs(freq-10e6));
[~,idx2] = min(abs(freq-3.5e9));
IL_limit = NaN(1,length(freq));

if get(handles.chkbox_GMSL2_IL,'Value')==1
    IL_limit(idx0:idx1) = -1.1;
    IL_limit(idx1:idx2) = -(0.2+0.28*sqrt(freq(idx1:idx2)/1E6)+0.8*freq(idx1:idx2)/1E9);
    data.GMSLILLimitPlot = plot(freq, IL_limit, '--r');
else
    delete(data.GMSLILLimitPlot);
end

guidata(hObject,data);


% --- Executes on button press in chkbox_FPD4_IL.
function chkbox_FPD4_IL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_FPD4_IL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_FPD4_IL
data=guidata(hObject);

if get(handles.chkbox_FPD4_IL,'Value')==1
    freq=[1e6 5e6 10e6 50e6 100e6 500e6 1e9 2.1e9 3e9 3.25e9 4e9 4.25e9];
    FPD4_IL=[-1.47 -2.46 -2.68 -3.45 -5.254 -10.71 -15.72 -24.29 -29.73 -30.83 -29.543 -31.15];
    data.FPDILLimitPlot = plot(freq, FPD4_IL,'r+');
else
    delete(data.FPDILLimitPlot);
end

guidata(hObject,data);



% --- Executes on button press in chkbox_ASA_RL.
function chkbox_ASA_RL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_ASA_RL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_ASA_RL
data=guidata(hObject);
global freq;

% 0 0.5 2 3 4.5 6.5
[~,idx1] = min(abs(freq-0));
[~,idx2] = min(abs(freq-500e6));
[~,idx3] = min(abs(freq-2e9));
[~,idx4] = min(abs(freq-3e9));
[~,idx5] = min(abs(freq-4.5e9));
[~,idx6] = min(abs(freq-5e9));

yd = nan(1,numel(freq));
yd(idx1:idx2) = -17*ones(1,idx2-idx1+1);
yd(idx2:idx3) = -17+4.*(freq(idx2:idx3)./1e6-500)./1500;
yd(idx3:idx4) = -13+1.5.*(freq(idx3:idx4)./1e6-2000)./1000;
yd(idx4:idx5) = -11.5+2.5.*(freq(idx4:idx5)./1e6-3000)./1500;
yd(idx5:idx6) = -9*ones(1,idx6-idx5+1);

if get(handles.chkbox_ASA_RL,'Value')==1
    data.ASALimitPlot = plot(freq, yd, '--r');
else
    delete(data.ASALimitPlot);
end

guidata(hObject,data);


%% --- Print specified axis only
% NB: Accept same arguments as 'print' except for first one which now is an axis.
function [] = printAxis(ax, varargin)
    % Create a temporary figure
    if isdeployed
        visibility = 'off';
    else
        visibility = 'on';
    end
    tempFigure = figure('Visible', visibility, 'Position', [10, 10, 960, 540]);
    cuo = onCleanup(@()clearTempFigure(tempFigure)); % Just to be sure to destroy the figure

    % Copy selected axis to the temporary figure
    newAx = copyobj(ax, tempFigure);

    % Make it fill whole figure space
    set(newAx, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);

    %% This would better fill the image but may lead to different sizes
    %% of the coordinate system when units/labels change.
    % outerpos = newAx.OuterPosition;
    % ti = newAx.TightInset; 
    % left = outerpos(1) + ti(1);
    % bottom = outerpos(2) + ti(2);
    % ax_width = outerpos(3) - ti(1) - ti(3);
    % ax_height = outerpos(4) - ti(2) - ti(4);
    % newAx.Position = [left bottom ax_width ax_height];
    %%

    % Print temporary figure
    print(tempFigure, varargin{1:end});        


function [] = clearTempFigure(h)
    if (ishandle(h)), delete(h); end



function edit_cable2_impmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable2_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable2_impmin as text
%        str2double(get(hObject,'String')) returns contents of edit_cable2_impmin as a double


% --- Executes during object creation, after setting all properties.
function edit_cable2_impmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable2_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable2_steps_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable2_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable2_steps as text
%        str2double(get(hObject,'String')) returns contents of edit_cable2_steps as a double


% --- Executes during object creation, after setting all properties.
function edit_cable2_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable2_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable2_impmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable2_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable2_impmax as text
%        str2double(get(hObject,'String')) returns contents of edit_cable2_impmax as a double


% --- Executes during object creation, after setting all properties.
function edit_cable2_impmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable2_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable3_impmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable3_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable3_impmin as text
%        str2double(get(hObject,'String')) returns contents of edit_cable3_impmin as a double


% --- Executes during object creation, after setting all properties.
function edit_cable3_impmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable3_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable3_steps_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable3_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable3_steps as text
%        str2double(get(hObject,'String')) returns contents of edit_cable3_steps as a double


% --- Executes during object creation, after setting all properties.
function edit_cable3_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable3_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable3_impmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable3_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable3_impmax as text
%        str2double(get(hObject,'String')) returns contents of edit_cable3_impmax as a double


% --- Executes during object creation, after setting all properties.
function edit_cable3_impmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable3_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable4_impmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable4_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable4_impmin as text
%        str2double(get(hObject,'String')) returns contents of edit_cable4_impmin as a double


% --- Executes during object creation, after setting all properties.
function edit_cable4_impmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable4_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable4_steps_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable4_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable4_steps as text
%        str2double(get(hObject,'String')) returns contents of edit_cable4_steps as a double


% --- Executes during object creation, after setting all properties.
function edit_cable4_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable4_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable4_impmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable4_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable4_impmax as text
%        str2double(get(hObject,'String')) returns contents of edit_cable4_impmax as a double


% --- Executes during object creation, after setting all properties.
function edit_cable4_impmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable4_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable5_impmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable5_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable5_impmin as text
%        str2double(get(hObject,'String')) returns contents of edit_cable5_impmin as a double


% --- Executes during object creation, after setting all properties.
function edit_cable5_impmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable5_impmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable5_steps_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable5_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable5_steps as text
%        str2double(get(hObject,'String')) returns contents of edit_cable5_steps as a double


% --- Executes during object creation, after setting all properties.
function edit_cable5_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable5_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_cable5_impmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cable5_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_cable5_impmax as text
%        str2double(get(hObject,'String')) returns contents of edit_cable5_impmax as a double


% --- Executes during object creation, after setting all properties.
function edit_cable5_impmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_cable5_impmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on btn_cable2 and none of its controls.
function btn_cable2_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to btn_cable2 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on btn_cable3 and none of its controls.
function btn_cable3_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to btn_cable3 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on btn_cable4 and none of its controls.
function btn_cable4_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to btn_cable4 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on btn_cable5 and none of its controls.
function btn_cable5_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to btn_cable5 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axis_handle = gca;
printAxis(axis_handle,'-clipboard','-dbitmap')


% --- Executes on button press in checkbox27.
function checkbox27_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox27
if get(hObject,'Value') == 1
    set(gca, 'xscale', 'log');
else
    set(gca, 'xscale', 'linear');
end


% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7
value    = get(hObject, 'Value');
userdata = get(hObject, 'UserData');
if ~userdata(value)
   % Reject callback.
   set(hObject, 'value', find(userdata, 1));
   return;
end

global CableTable;
global CableTableBase

if strcmp(handles.popupmenu7.String(value), 'single-ended')
    value = 'coax';
else
    value = handles.popupmenu7.String(value);
end

CableTable = CableTableBase(strcmp(CableTableBase.type,value),:);
name2 = strcat(num2str(CableTable.uid), {' '}, CableTable.man, {' '}, CableTable.name, {' ('}, CableTable.type, {')'});

handles.popup_cable1.String = name2';
handles.popup_cable2.String = name2';
handles.popup_cable3.String = name2';
handles.popup_cable4.String = name2';
handles.popup_cable5.String = name2';



% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_headerP1.
function btn_headerP1_Callback(hObject, eventdata, handles)
% hObject    handle to btn_headerP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_headerP1.BackgroundColor == [1 0 0]
    set(handles.btn_headerP1,'BackgroundColor','green');
else
    set(handles.btn_headerP1,'BackgroundColor','red');
end

% --- Executes on button press in btn_inliner1_toggle.
function btn_inliner1_toggle_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner1_toggle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_inliner1_toggle.BackgroundColor == [1 0 0]
    set(handles.btn_inliner1_toggle,'BackgroundColor','green');
else
    set(handles.btn_inliner1_toggle,'BackgroundColor','red');
end


% --- Executes on button press in btn_inliner2_toggle.
function btn_inliner2_toggle_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner2_toggle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_inliner2_toggle.BackgroundColor == [1 0 0]
    set(handles.btn_inliner2_toggle,'BackgroundColor','green');
else
    set(handles.btn_inliner2_toggle,'BackgroundColor','red');
end


% --- Executes on button press in btn_inliner3_toggle.
function btn_inliner3_toggle_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner3_toggle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_inliner3_toggle.BackgroundColor == [1 0 0]
    set(handles.btn_inliner3_toggle,'BackgroundColor','green');
else
    set(handles.btn_inliner3_toggle,'BackgroundColor','red');
end


% --- Executes on button press in btn_inliner4_toggle.
function btn_inliner4_toggle_Callback(hObject, eventdata, handles)
% hObject    handle to btn_inliner4_toggle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_inliner4_toggle.BackgroundColor == [1 0 0]
    set(handles.btn_inliner4_toggle,'BackgroundColor','green');
else
    set(handles.btn_inliner4_toggle,'BackgroundColor','red');
end


% --- Executes on button press in btn_headerP2.
function btn_headerP2_Callback(hObject, eventdata, handles)
% hObject    handle to btn_headerP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.btn_headerP2.BackgroundColor == [1 0 0]
    set(handles.btn_headerP2,'BackgroundColor','green');
else
    set(handles.btn_headerP2,'BackgroundColor','red');
end


function setCable(cableNumber, handles, cableSegment, impedances)

global CableTable;

imp_min = 50;
imp_max = 50;
num_steps = 1;

imp_data = CableTable.impedance(cableNumber);
imp_data = imp_data{1};
if numel(imp_data) == 1
    imp_min = imp_data;
    imp_max = imp_min;
    num_steps = 1;
elseif numel(imp_data) == 2
    imp_min = imp_data(1)./(1+imp_data(2)/100);
    imp_max = imp_data(1).*(1+imp_data(2)/100);
    num_steps = 5;
elseif numel(imp_data) == 3
    imp_min = imp_data(1);
    imp_max = imp_data(3);
    num_steps = 5;
else
    % if type STP/SPP 100 else 50
end


edit_steps = ['edit_cable' num2str(cableSegment) '_steps'];
edit_impmin = ['edit_cable' num2str(cableSegment) '_impmin'];
edit_impmax = ['edit_cable' num2str(cableSegment) '_impmax'];

handles.(edit_steps).String = num_steps;
handles.(edit_impmin).String = imp_min;
handles.(edit_impmax).String = imp_max;



% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
hObject.Name = ['Topology Engineering App - ' fileread('version.txt')];



% --- Executes on button press in check_envelope.
function check_envelope_Callback(hObject, eventdata, handles)
% hObject    handle to check_envelope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_envelope
global Results;
data=guidata(hObject);
global freq;

if get(hObject,'Value')
    hObject.Value = 1;
    handles.slider_envelope.Visible = 'On';
    handles.envelope_type.Visible = 'On';
    
    if ~exist('Results.s11_max')
        updateEnvelope(hObject, handles);
    end
else
    handles.slider_envelope.Visible = 'Off';
    handles.envelope_type.Visible = 'Off';
    hObject.Value = 0;
    exist('data.p_s11_max');
    hold off;
    delete(data.p_s11_max);
    hold on;
    guidata(hObject,data);
end



function updateEnvelope(hObject, handles)

    global Results;
    data=guidata(hObject);
    global freq;

    if isfield(data, 'p_s11_max')
        hold off;
        delete(data.p_s11_max);
        hold on;
    end
    
    if strcmp(handles.envelope_type.String{handles.envelope_type.Value}, 'right')
        Results.s11_sort = sort(Results.s22, 2, 'ComparisonMethod', 'abs');
    elseif strcmp(handles.envelope_type.String{handles.envelope_type.Value}, 'both')
        Results.s11_sort = sort([Results.s11 Results.s22], 2, 'ComparisonMethod', 'abs');
    else
        Results.s11_sort = sort(Results.s11, 2, 'ComparisonMethod', 'abs');
    end
        
%        Results.s11_sort = sort([Results.s11 Results.s22], 2, 'ComparisonMethod', 'abs');
        Results.s11_max = abs(Results.s11_sort(:,end));
        
        numsrc = size(Results.s11_sort, 2);
        num50 = floor(numsrc/2);
        num75 = floor(numsrc*0.75);

        Results.s11_50 = abs(Results.s11_sort(:,num50));
        Results.s11_75 = abs(Results.s11_sort(:,num75));
        
        handles_slider_envelope_Value = 60;

        [maxValue, maxIndex] = findpeaks(Results.s11_max,'MinPeakDistance',handles_slider_envelope_Value);
        [Value50, Index50] = findpeaks(Results.s11_50,'MinPeakDistance',handles_slider_envelope_Value);
        [Value75, Index75] = findpeaks(Results.s11_75,'MinPeakDistance',handles_slider_envelope_Value);

        slider = false;
        
        if numel(maxIndex) < 20 | maxIndex(1) > 0.1*numel(freq) | maxIndex(end) < 0.9*numel(freq)
            z1 = Results.s11_max;
        else
            z1 = interp1(freq(maxIndex),maxValue,freq,'linear');
            z1(isnan(z1)) = Results.s11_max(isnan(z1));
            slider = true;
        end

        if numel(Index75) < 20 | Index75(1) > 0.1*numel(freq) | Index75(end) < 0.9*numel(freq)
            z2 = Results.s11_75;
        else
            z2 = interp1(freq(Index75),Value75,freq,'linear');
            z2(isnan(z2)) = Results.s11_75(isnan(z2));
            slider = true;
        end
        
        if numel(Index50) < 20 | Index50(1) > 0.1*numel(freq) | Index50(end) < 0.9*numel(freq)
            z3 = Results.s11_50;
        else
            z3 = interp1(freq(Index50),Value50,freq,'linear');
            z3(isnan(z3)) = Results.s11_50(isnan(z3));
            slider = true;
        end
        
        if slider == true
            handles.slider_envelope.Visible = 'On';
        end

        z = sort([20*log10(z1) 20*log10(z2) 20*log10(z3)], 2, 'descend');

        Results.s11_env = z1;
%        data.p_s11_max = plot(freq, z(:,1), freq, z(:,2), freq, z(:,3));
        data.p_s11_max = fill([freq; flip(freq)], [z(:,2); flip(z(:,1));], [0.6, 0.8, 1.0], 'EdgeColor', 'none');
        data.p_s11_max = [data.p_s11_max; fill([freq; flip(freq)], [z(:,3); flip(z(:,2));], [0.3, 0.65, 1.0], 'EdgeColor', 'none')];
        data.p_s11_max = [data.p_s11_max; fill([freq(1); freq; freq(end)], [-200; z(:,3); -200], [0, 0.5, 1.0], 'EdgeColor', 'none')];

guidata(hObject,data);



% --- Executes on slider movement.
function slider_envelope_Callback(hObject, eventdata, handles)
% hObject    handle to slider_envelope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
hObject.Tooltip = num2str(hObject.Value);

data=guidata(hObject);
delete(data.p_s11_max);
guidata(hObject,data);
updateEnvelope(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider_envelope_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_envelope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider_envelope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_envelope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in checkbox30.
function checkbox30_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox30


% --- Executes on button press in chk_flip1.
function chk_flip1_Callback(hObject, eventdata, handles)
% hObject    handle to chk_flip1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chk_flip1
global freq;
global connector_db;
global connector_flip_state;

%inliner1_data = connector_db.(['con_' num2str(hObject.Value)]).abcd;
inliner1_data = evalin('base', 'inliner1_data');

if handles.chk_flip1.Value == 1 ~= connector_flip_state.inline1
    connector_flip_state.inline1 = handles.chk_flip1.Value;
    for ni = 1:numel(inliner1_data)
        temp = inliner1_data{ni}.Parameters;
        inliner1_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner1_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner1_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner1_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end

assignin('base', 'inliner1_data', inliner1_data)
set(handles.btn_inliner1_toggle,'BackgroundColor','green');
assignin('base', 'inliner1_noFiles', length(inliner1_data))



% --- Executes on selection change in envelope_type.
function envelope_type_Callback(hObject, eventdata, handles)
% hObject    handle to envelope_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns envelope_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from envelope_type
data=guidata(hObject);

if isfield(data, 'p_s11_max')
    hold off;
    delete(data.p_s11_max);
    hold on;
end

guidata(hObject,data);

updateEnvelope(hObject, handles);



% --- Executes during object creation, after setting all properties.
function envelope_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to envelope_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_irr_env.
function check_irr_env_Callback(hObject, eventdata, handles)
% hObject    handle to check_irr_env (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_irr_env
global Results;
data=guidata(hObject);
global freq;

if get(hObject,'Value')
    hObject.Value = 1;
    handles.slider_IRR_env.Visible = 'On';
    handles.IRR_env_type.Visible = 'On';
    
    if ~exist('Results.irr_env')
        updateIrrEnvelope(hObject, handles);
    end
else
    handles.slider_IRR_env.Visible = 'Off';
    handles.IRR_env_type.Visible = 'Off';
    hObject.Value = 0;
    if isfield(data, 'p_irr_env') == 1
        hold off;
        delete(data.p_irr_env);
        hold on;
    end
    guidata(hObject,data);
end


function updateIrrEnvelope(hObject, handles)

    global Results;
    data=guidata(hObject);
    global freq;

    if isfield(data, 'p_irr_env')
        hold off;
        delete(data.p_irr_env);
        hold on;
    end
    
    % calculate IRR
    s11_dB=20*log10(abs(Results.s11));
%    s12_dB=20*log10(abs(Results.s12));
    s21_dB=20*log10(abs(Results.s21));
    s12_dB=s21_dB;
    s22_dB=20*log10(abs(Results.s22));

    if strcmp(handles.IRR_env_type.String{handles.IRR_env_type.Value}, 'right')
        [irr_sort irr_sort_perm] = sort(s21_dB - s22_dB, 2);
        snn_sort = zeros(size(irr_sort));
        snm_sort = zeros(size(irr_sort));
        for r = 1:size(irr_sort, 1)
            snn_sort(r,:) = s22_dB(r, irr_sort_perm(r,:));
            snm_sort(r,:) = s21_dB(r, irr_sort_perm(r,:));
        end
    elseif strcmp(handles.IRR_env_type.String{handles.IRR_env_type.Value}, 'both')
        nm = [s12_dB s21_dB];
        nn = [s11_dB s22_dB];
        [irr_sort irr_sort_perm] = sort(nm - nn, 2);
        snn_sort = zeros(size(irr_sort));
        snm_sort = zeros(size(irr_sort));
        for r = 1:size(irr_sort, 1)
            snn_sort(r,:) = nn(r, irr_sort_perm(r,:));
            snm_sort(r,:) = nm(r, irr_sort_perm(r,:));
        end
    else
        [irr_sort irr_sort_perm] = sort(s12_dB - s11_dB, 2);
        snn_sort = zeros(size(irr_sort));
        snm_sort = zeros(size(irr_sort));
        for r = 1:size(irr_sort, 1)
            snn_sort(r,:) = s11_dB(r, irr_sort_perm(r,:));
            snm_sort(r,:) = s12_dB(r, irr_sort_perm(r,:));
        end
    end

    numsrc = size(irr_sort, 2);
    num50 = ceil(numsrc/2)+1;
    num75 = ceil(numsrc*0.25)+1;

    snn_dB_min = snn_sort(:,1);
    snm_dB_min = snm_sort(:,1);
    snn_dB_50 = snn_sort(:,num50);
    snm_dB_50 = snm_sort(:,num50);
    snn_dB_75 = snn_sort(:,num75);
    snm_dB_75 = snm_sort(:,num75);

    result_names = {'irr_min', 'irr_50', 'irr_75'};
    
    ws = warning('off','all');  % Turn off warning of polyfit
    
    p1 = polyfit(freq,snn_dB_min,20);
    snn_fit_dB_min = polyval(p1,freq);
    irr_fit_min = snm_dB_min-snn_fit_dB_min;

    p1 = polyfit(freq,snn_dB_75,20);
    snn_fit_dB_75 = polyval(p1,freq);
    irr_fit_75 = snm_dB_75-snn_fit_dB_75;

    p1 = polyfit(freq,snn_dB_50,20);
    snn_fit_dB_50 = polyval(p1,freq);
    irr_fit_50 = snm_dB_50-snn_fit_dB_50;

    warning(ws) % Turn warning back on.

% Results.irr21 = IRR_21_fit;
% Results.irr12 = IRR_12_fit;
%    
%     if strcmp(handles.IRR_env_type.String{handles.IRR_env_type.Value}, 'right')
%         Results.irr_sort = sort(Results.irr12, 2);
%     elseif strcmp(handles.IRR_env_type.String{handles.IRR_env_type.Value}, 'both')
%         Results.irr_sort = sort([Results.irr12 Results.irr21], 2);
%     else
%         Results.irr_sort = sort(Results.irr21, 2);
%     end

%        Results.s11_sort = sort([Results.s11 Results.s22], 2, 'ComparisonMethod', 'abs');

%        [maxValue, maxIndex] = findpeaks(-Results.irr_min,'MinPeakDistance',handles.slider_IRR_env.Value);
%        [Value50, Index50] = findpeaks(-Results.irr_50,'MinPeakDistance',handles.slider_IRR_env.Value);
%        [Value75, Index75] = findpeaks(-Results.irr_75,'MinPeakDistance',handles.slider_IRR_env.Value);

%        maxValue = -maxValue;
%        Value50 = -Value50;
%        Value75 = -Value75;
        
%        slider = false;
        
%        if numel(maxIndex) < 20 | maxIndex(1) > 0.1*numel(freq) | maxIndex(end) < 0.9*numel(freq)
            z1 = irr_fit_min;
%        else
%            z1 = interp1(freq(maxIndex),maxValue,freq,'linear');
%            z1(isnan(z1)) = Results.irr_min(isnan(z1));
%            slider = true;
%        end

%        if numel(Index75) < 20 | Index75(1) > 0.1*numel(freq) | Index75(end) < 0.9*numel(freq)
            z2 = irr_fit_75;
%        else
%            z2 = interp1(freq(Index75),Value75,freq,'linear');
%            z2(isnan(z2)) = Results.irr_75(isnan(z2));
%            slider = true;
%        end
        
%        if numel(Index50) < 20 | Index50(1) > 0.1*numel(freq) | Index50(end) < 0.9*numel(freq)
            z3 = irr_fit_50;
%        else
%            z3 = interp1(freq(Index50),Value50,freq,'linear');
%            z3(isnan(z3)) = Results.irr_50(isnan(z3));
%            slider = true;
%        end
        
%        if slider == true
%            handles.slider_IRR_env.Visible = 'On';
%        end

        z = sort([z1 z2 z3], 2, 'descend');

        Results.irr_env = z1;
%        data.p_s11_max = plot(freq, z(:,1), freq, z(:,2), freq, z(:,3));
        data.p_irr_env = fill([freq; flip(freq)], [z(:,2); flip(z(:,3));], [0.9, 0.9, 0.35], 'EdgeColor', 'none');
        data.p_irr_env = [data.p_irr_env; fill([freq; flip(freq)], [z(:,1); flip(z(:,2));], [0.8, 0.8, 0.3], 'EdgeColor', 'none')];
        data.p_irr_env = [data.p_irr_env; fill([freq(1); freq; freq(end)], [200; z(:,1); 200], [0.6, 0.6, 0.2], 'EdgeColor', 'none')];

guidata(hObject,data);


% --- Executes on selection change in IRR_env_type.
function IRR_env_type_Callback(hObject, eventdata, handles)
% hObject    handle to IRR_env_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns IRR_env_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IRR_env_type
data=guidata(hObject);

if isfield(data, 'p_irr_env')
    hold off;
    delete(data.p_irr_env);
    hold on;
end

guidata(hObject,data);

updateIrrEnvelope(hObject, handles);



% --- Executes during object creation, after setting all properties.
function IRR_env_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IRR_env_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_IRR_env_Callback(hObject, eventdata, handles)
% hObject    handle to slider_IRR_env (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
hObject.Tooltip = num2str(hObject.Value);

data=guidata(hObject);
delete(data.p_irr_env);
guidata(hObject,data);
updateIrrEnvelope(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider_IRR_env_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_IRR_env (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in pop_header_l.
function pop_header_l_Callback(hObject, eventdata, handles)
% hObject    handle to pop_header_l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_header_l contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_header_l
global freq;
global connector_db;
global header_index;

headerP1_data = connector_db.(['con_' num2str(header_index(hObject.Value))]).abcd;

if strcmp(connector_db.(['con_' num2str(header_index(hObject.Value))]).cable{2}, 'PCB')
    for ni = 1:numel(headerP1_data)
        temp = headerP1_data{ni}.Parameters;
        headerP1_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        headerP1_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        headerP1_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        headerP1_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end
assignin('base', 'headerP1_data', headerP1_data)
handles.btn_headerP1.BackgroundColor = 'green';
assignin('base', 'headerP1_noFiles', length(headerP1_data))



% --- Executes during object creation, after setting all properties.
function pop_header_l_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_header_l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
global freq;
global connector_db;
global header_index;

headerP1_data = connector_db.(['con_' num2str(header_index(1))]).abcd;

if strcmp(connector_db.(['con_' num2str(header_index(1))]).cable{2}, 'PCB')
    for ni = 1:numel(headerP1_data)
        temp = headerP1_data{ni}.Parameters;
        headerP1_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        headerP1_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        headerP1_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        headerP1_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end
assignin('base', 'headerP1_data', headerP1_data)
assignin('base', 'headerP1_noFiles', length(headerP1_data))



% --- Executes on selection change in pop_inline_1.
function pop_inline_1_Callback(hObject, eventdata, handles)
% hObject    handle to pop_inline_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_inline_1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_inline_1
global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner1_data = connector_db.(['con_' num2str(inline_index(hObject.Value))]).abcd;

if handles.chk_flip1.Value == 1
    for ni = 1:numel(inliner1_data)
        temp = inliner1_data{ni}.Parameters;
        inliner1_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner1_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner1_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner1_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
    connector_flip_state.inline1 = 1;
else
    connector_flip_state.inline1 = 0;
end

assignin('base', 'inliner1_data', inliner1_data)
set(handles.btn_inliner1_toggle,'BackgroundColor','green');
assignin('base', 'inliner1_noFiles', length(inliner1_data))



% --- Executes during object creation, after setting all properties.
function pop_inline_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_inline_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner1_data = connector_db.(['con_' num2str(inline_index(1))]).abcd;
connector_flip_state.inline1 = 0;

assignin('base', 'inliner1_data', inliner1_data)
assignin('base', 'inliner1_noFiles', length(inliner1_data))



% --- Executes on selection change in pop_inline_2.
function pop_inline_2_Callback(hObject, eventdata, handles)
% hObject    handle to pop_inline_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_inline_2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_inline_2
global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner2_data = connector_db.(['con_' num2str(inline_index(hObject.Value))]).abcd;

if handles.chk_flip2.Value == 1
    for ni = 1:numel(inliner2_data)
        temp = inliner2_data{ni}.Parameters;
        inliner2_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner2_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner2_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner2_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
    connector_flip_state.inline2 = 1;
else
    connector_flip_state.inline2 = 0;
end

assignin('base', 'inliner2_data', inliner2_data)
set(handles.btn_inliner2_toggle,'BackgroundColor','green');
assignin('base', 'inliner2_noFiles', length(inliner2_data))


% --- Executes during object creation, after setting all properties.
function pop_inline_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_inline_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner2_data = connector_db.(['con_' num2str(inline_index(1))]).abcd;
connector_flip_state.inline2 = 0;

assignin('base', 'inliner2_data', inliner2_data)
assignin('base', 'inliner2_noFiles', length(inliner2_data))



% --- Executes on selection change in pop_inline_3.
function pop_inline_3_Callback(hObject, eventdata, handles)
% hObject    handle to pop_inline_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_inline_3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_inline_3
global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner3_data = connector_db.(['con_' num2str(inline_index(hObject.Value))]).abcd;

if handles.chk_flip3.Value == 1
    for ni = 1:numel(inliner3_data)
        temp = inliner3_data{ni}.Parameters;
        inliner3_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner3_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner3_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner3_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
    connector_flip_state.inline3 = 1;
else
    connector_flip_state.inline3 = 0;
end

assignin('base', 'inliner3_data', inliner3_data)
set(handles.btn_inliner3_toggle,'BackgroundColor','green');
assignin('base', 'inliner3_noFiles', length(inliner3_data))



% --- Executes during object creation, after setting all properties.
function pop_inline_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_inline_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner3_data = connector_db.(['con_' num2str(inline_index(1))]).abcd;
connector_flip_state.inline3 = 0;

assignin('base', 'inliner3_data', inliner3_data)
assignin('base', 'inliner3_noFiles', length(inliner3_data))



% --- Executes on selection change in pop_inline_4.
function pop_inline_4_Callback(hObject, eventdata, handles)
% hObject    handle to pop_inline_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_inline_4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_inline_4
global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner4_data = connector_db.(['con_' num2str(inline_index(hObject.Value))]).abcd;

if handles.chk_flip4.Value == 1
    for ni = 1:numel(inliner4_data)
        temp = inliner4_data{ni}.Parameters;
        inliner4_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner4_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner4_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner4_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
    connector_flip_state.inline4 = 1;
else
    connector_flip_state.inline4 = 0;
end

assignin('base', 'inliner4_data', inliner4_data)
set(handles.btn_inliner4_toggle,'BackgroundColor','green');
assignin('base', 'inliner4_noFiles', length(inliner4_data))



% --- Executes during object creation, after setting all properties.
function pop_inline_4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_inline_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global freq;
global connector_db;
global connector_flip_state;
global inline_index;

inliner4_data = connector_db.(['con_' num2str(inline_index(1))]).abcd;
connector_flip_state.inline4 = 0;

assignin('base', 'inliner4_data', inliner4_data)
assignin('base', 'inliner4_noFiles', length(inliner4_data))



% --- Executes on selection change in pop_header_r.
function pop_header_r_Callback(hObject, eventdata, handles)
% hObject    handle to pop_header_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_header_r contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_header_r
global freq;
global connector_db;
global header_index;

headerP2_data = connector_db.(['con_' num2str(header_index(hObject.Value))]).abcd;

if strcmp(connector_db.(['con_' num2str(header_index(hObject.Value))]).cable{1}, 'PCB')
    for ni = 1:numel(headerP2_data)
        temp = headerP2_data{ni}.Parameters;
        headerP2_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        headerP2_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        headerP2_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        headerP2_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end

assignin('base', 'headerP2_data', headerP2_data)
set(handles.btn_headerP2,'BackgroundColor','green');
assignin('base', 'headerP2_noFiles', length(headerP2_data))


% --- Executes during object creation, after setting all properties.
function pop_header_r_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_header_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global freq;
global connector_db;
global header_index;

headerP2_data = connector_db.(['con_' num2str(header_index(1))]).abcd;

if strcmp(connector_db.(['con_' num2str(header_index(1))]).cable{1}, 'PCB')
    for ni = 1:numel(headerP2_data)
        temp = headerP2_data{ni}.Parameters;
        headerP2_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        headerP2_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        headerP2_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        headerP2_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end

assignin('base', 'headerP2_data', headerP2_data)
assignin('base', 'headerP2_noFiles', length(headerP2_data))



% --- Executes on button press in chk_flip2.
function chk_flip2_Callback(hObject, eventdata, handles)
% hObject    handle to chk_flip2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chk_flip2
global freq;
global connector_db;
global connector_flip_state;

%inliner1_data = connector_db.(['con_' num2str(hObject.Value)]).abcd;
inliner2_data = evalin('base', 'inliner2_data');

if handles.chk_flip2.Value == 1 ~= connector_flip_state.inline2
    connector_flip_state.inline2 = handles.chk_flip2.Value;
    for ni = 1:numel(inliner2_data)
        temp = inliner2_data{ni}.Parameters;
        inliner2_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner2_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner2_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner2_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end

assignin('base', 'inliner2_data', inliner2_data)
set(handles.btn_inliner2_toggle,'BackgroundColor','green');
assignin('base', 'inliner2_noFiles', length(inliner2_data))



% --- Executes on button press in chk_flip3.
function chk_flip3_Callback(hObject, eventdata, handles)
% hObject    handle to chk_flip3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chk_flip3
global freq;
global connector_db;
global connector_flip_state;

%inliner1_data = connector_db.(['con_' num2str(hObject.Value)]).abcd;
inliner3_data = evalin('base', 'inliner3_data');

if handles.chk_flip3.Value == 1 ~= connector_flip_state.inline3
    connector_flip_state.inline3 = handles.chk_flip3.Value;
    for ni = 1:numel(inliner3_data)
        temp = inliner3_data{ni}.Parameters;
        inliner3_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner3_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner3_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner3_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end

assignin('base', 'inliner3_data', inliner3_data)
set(handles.btn_inliner3_toggle,'BackgroundColor','green');
assignin('base', 'inliner3_noFiles', length(inliner3_data))


% --- Executes on button press in chk_flip4.
function chk_flip4_Callback(hObject, eventdata, handles)
% hObject    handle to chk_flip4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chk_flip4
global freq;
global connector_db;
global connector_flip_state;

%inliner1_data = connector_db.(['con_' num2str(hObject.Value)]).abcd;
inliner4_data = evalin('base', 'inliner4_data');

if handles.chk_flip4.Value == 1 ~= connector_flip_state.inline4
    connector_flip_state.inline4 = handles.chk_flip4.Value;
    for ni = 1:numel(inliner4_data)
        temp = inliner4_data{ni}.Parameters;
        inliner4_data{ni}.Parameters(1,1,:) = temp(2,2,:);
        inliner4_data{ni}.Parameters(1,2,:) = temp(1,2,:);
        inliner4_data{ni}.Parameters(2,1,:) = temp(2,1,:);
        inliner4_data{ni}.Parameters(2,2,:) = temp(1,1,:);
    end
end

assignin('base', 'inliner4_data', inliner4_data)
set(handles.btn_inliner4_toggle,'BackgroundColor','green');
assignin('base', 'inliner4_noFiles', length(inliner4_data))


% --- Executes on button press in chkbox_NGAUTO_IL.
function chkbox_NGAUTO_IL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_NGAUTO_IL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_NGAUTO_IL
data=guidata(hObject);
global freq;

[~,idx1] = min(abs(freq-1e6));
[~,idx2] = min(abs(freq-4e9));

yd = nan(1,numel(freq));
yd(idx1:idx2) = -0.002*freq(idx1:idx2)./1e6-0.68*(freq(idx1:idx2)./1e6).^0.45;

if get(handles.chkbox_NGAUTO_IL,'Value')==1
    data.NGAUTOILLimitPlot = plot(freq, yd, '--r');
else
    delete(data.NGAUTOILLimitPlot);
end

guidata(hObject,data);



% --- Executes on button press in chkbox_ASA_IL.
function chkbox_ASA_IL_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_ASA_IL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_ASA_IL
data=guidata(hObject);
global freq;

[~,idx1] = min(abs(freq-10e6));
[~,idx2] = min(abs(freq-5000e6));

yd = nan(1,numel(freq));
yd(idx1:idx2) = 0.15*(1.3-0.0115*freq(idx1:idx2)./1e6-2*sqrt(freq(idx1:idx2)./1e6)-3.79./sqrt(freq(idx1:idx2)./1e6));

if get(handles.chkbox_ASA_IL,'Value')==1
    data.ASAILLimitPlot = plot(freq, yd, '--r');
else
    delete(data.ASAILLimitPlot);
end

guidata(hObject,data);
