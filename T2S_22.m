% T2S_22.m, version 0.1, 2-16-2018
% bbergner@te.com, eric.dibiaso@te.com
%
% transforms chain (t-) parameter to s-parameter, expects 2x2 t-matrix vector
%
% all dimensions given and expected in SI base units

% arguments

    %  T      2x2 x NF matrix

% return values

    %  S      2x2 matrix for each frequency step iy:
    %             S11=S((iy*2-1),1)     S12=S((iy*2-1),2)
    %             S21=S((iy*2),1)       S22=S((iy*2),2)

function S=T2S_22(T)
    dim=size(T);
    for iy=1:(dim(1,1)/2)
        T_=T((iy*2-1):(iy*2),:);    % sub matrix for current frequency step
        S_=[(T_(1,2)/T_(2,2)), ...
            ((T_(1,1)*T_(2,2)-T_(1,2)*T_(2,1))/T_(2,2)); ...
            (1/T_(2,2)), ...
            (-T_(2,1)/T_(2,2))];
        S((iy*2-1):(iy*2),1:2)=S_;
    end
end
