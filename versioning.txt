General version numbering:

  f  - feature release number - as the name says; starts with 0
  .
  m  - maintenance release number - only miner features, but e.g.
       compatibility improvements etc. Starts with 0 unless f==0, then it
       starts with 1
  .
  b  - bugfix release number - only fixes, no new features
[[a  - alpha release - early release to test some aspects of the sw
  b  - beta release - feature complete test release
  rc - release candidate
 ]#  - number of the particular pre-release, starting with 0
]

0.* releases are internal or external development releases and release 1.*
is the first official public release.

There are no special customer versions, features that should not be
available to some customers must be switched by compiler directives or
configuration flags (build time).

First release is 0.1.0
