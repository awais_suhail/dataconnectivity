function abcd_par=casc_TS_single(freq, abcd_left, abcs_right)
    
    for i = 1:length(freq)
        ABCDcasc(:,:,i) = abcd_left.Parameters(:,:,i)*abcs_right.Parameters(:,:,i);
    end

    
    abcd_par = abcdparameters(ABCDcasc,freq);
end