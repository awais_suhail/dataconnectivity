% S2T_22.m, version 0.1, 2-16-2018, 
% bbergner@te.com, eric.dibiaso@te.com
%
% transforms s-parameter to chain (t) parameter, expects 2x2 s-matrix vector
%
% all dimensions given and expected in SI base units

% arguments

    %  S      2x2 x NF matrix

% return values

    %  T      2x2 matrix for each frequency step iy:
    %             T11=T((iy*2-1),1)     T12=T((iy*2-1),2)
    %             T21=T((iy*2),1)       T22=T((iy*2),2)

function T=S2T_22(S)
    dim=size(S);
    for iy=1:(dim(1,1)/2)
        S_=S((iy*2-1):(iy*2),:);    % sub matrix for current frequency step
        T_=[((S_(1,2)*S_(2,1)-S_(1,1)*S_(2,2))/S_(2,1)), ...
            (S_(1,1)/S_(2,1)); ...
            (-S_(2,2)/S_(2,1)), ...
            (1/S_(2,1))];
        T((iy*2-1):(iy*2),1:2)=T_;
    end
end
