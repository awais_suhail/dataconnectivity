TODO

- RG174 Kabel einpflegen
- Datenstruktur f�r Plotergebnisse einf�hren
- Nominal-Kurve + Worst-Case f�r jeden Punkt darstellen
- F�r jeden Bereich Quantile darstellen
- Auswahl der Komponenten ohne Dateidialog
- Ergebnisausgabe farblich codieren
- Performanz verbessern??
  - Ist das Lesen der Datei das Bottleneck?
    - Als Bin�rdatei ablegen
  - Ist das Rechnen das Problem?
    - Punkte reduzieren
- Verifikation mit Messungen
- fig-File in BMW-Branch umbenennen

Neues Projekt:

- HSD einpflegen
